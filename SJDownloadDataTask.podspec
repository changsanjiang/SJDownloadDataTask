
Pod::Spec.new do |s|
  s.name             = 'SJDownloadDataTask'
  s.version          = '1.2.4'
  s.summary          = '下载器'

  s.description      = <<-DESC
        封装的NSURLSessionDataTask的下载器
                       DESC

  s.homepage         = 'https://gitee.com/changsanjiang/SJDownloadDataTask'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.source           = { :git => 'https://gitee.com/changsanjiang/SJDownloadDataTask.git', :tag => "v#{s.version}" }
  s.author           = { 'SanJiang' => 'changsanjiang@gmail.com' }

  s.ios.deployment_target = '8.0'

  s.source_files = 'SJDownloadDataTask/*.{h,m}'

  s.subspec 'Core' do |ss|
    ss.source_files = 'SJDownloadDataTask/Core/*.{h,m}'
    ss.subspec 'ResourceLoader' do |sss|
        sss.source_files = 'SJDownloadDataTask/Core/ResourceLoader/*.{h,m}'
        sss.resource = 'SJDownloadDataTask/Core/ResourceLoader/SJDownloadDataTask.bundle'
    end
  end

  s.dependency 'SJUIKit/ObserverHelper'

end
