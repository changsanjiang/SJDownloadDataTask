//
//  ViewController.m
//  SJDownloadDataTask
//
//  Created by 畅三江 on 2018/5/26.
//  Copyright © 2018年 畅三江. All rights reserved.
//

#import "ViewController.h"
#import "SJDownloadDataTask.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIProgressView *progress;
@property (nonatomic, weak, nullable) SJDownloadDataTask *task;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)cancel:(id)sender {
    [_task cancel];
}
- (IBAction)begin:(id)sender {
    __weak typeof(self) _self = self;
    SJDownloadDataTask *task = [SJDownloadDataTask downloadWithURLStr:@"http://audio.cdn.lanwuzhe.com/1492776280608c177" toPath:[NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"ddd.mp3"]] append:YES progress:^(SJDownloadDataTask * _Nonnull dataTask, float progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(_self) self = _self;
            if ( !self ) return ;
            self.progress.progress = progress;
        });
    } success:^(SJDownloadDataTask * _Nonnull dataTask) {
        
    } failure:^(SJDownloadDataTask * _Nonnull dataTask) {
        
    }];
    
    _task = task;
}
- (IBAction)restart:(id)sender {
    [_task restart];
}

@end
